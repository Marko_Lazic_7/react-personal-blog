import React from 'react'
import styles from "./Favorites.module.scss"

function Favorites() {
    return (
        <div className={styles.home}>
        <div className={styles.container}>
            <h2 className={styles.title}>Favorites</h2>
            <div className={styles.text}>Favorite food: <a href="https://hr.wikipedia.org/wiki/Pizza">Pizza</a> </div> 
            <div className={styles.text}>Favorite game: <a href="https://www.konami.com/wepes/2021/eu/en/ps4/">PES</a></div>
            <div className={styles.text}>Favorite pet: <a href="https://www.dailypaws.com/dogs-puppies/dog-breeds/boston-terrier">Boston Terrier</a></div>
            <div className={styles.text}>Favorite football club: <a href="https://www.realmadrid.com/en">Real Madrid</a></div>

        </div> 
    </div>
    )
}

export default Favorites
