import React from 'react'
import styles from "./Home.module.scss"

function Home() {
    return (
        <div className={styles.home}>
            <div className={styles.container}>
            <h2 className={styles.title}>Title</h2>
            <div className={styles.text}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam dicta aspernatur animi magnam unde ipsum, maiores voluptatibus placeat?
            Odio explicabo perferendis eum voluptatum, assumenda quasi reprehenderit beatae officia illum ea?</div>
        </div>
        <div className={styles.container}>
            <h2 className={styles.title}>Title</h2>
            <div className={styles.text}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam dicta aspernatur animi magnam unde ipsum, maiores voluptatibus placeat?
            Odio explicabo perferendis eum voluptatum, assumenda quasi reprehenderit beatae officia illum ea?</div>
        </div>
        <div className={styles.container}>
            <h2 className={styles.title}>Title</h2>
            <div className={styles.text}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam dicta aspernatur animi magnam unde ipsum, maiores voluptatibus placeat?
            Odio explicabo perferendis eum voluptatum, assumenda quasi reprehenderit beatae officia illum ea?</div>
        </div>
        <div className={styles.container}>
            <h2 className={styles.title}>Title</h2>
            <div className={styles.text}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam dicta aspernatur animi magnam unde ipsum, maiores voluptatibus placeat?
            Odio explicabo perferendis eum voluptatum, assumenda quasi reprehenderit beatae officia illum ea?</div>
        </div>
        </div>
    )
}

export default Home
