import React from 'react'
import styles from "./AboutMe.module.scss"

function AboutMe() {
    return (
        
    <div className={styles.home}>
        <div className={styles.container}>
            <h2 className={styles.title}>About Me</h2>
            <div className={styles.text}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam dicta aspernatur animi magnam unde ipsum, maiores voluptatibus placeat?
            Odio explicabo perferendis eum voluptatum, assumenda quasi reprehenderit beatae officia illum ea?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam dicta aspernatur animi magnam unde ipsum, maiores voluptatibus placeat?
            Odio explicabo perferendis eum voluptatum, assumenda quasi reprehenderit beatae officia illum ea?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam dicta aspernatur animi magnam unde ipsum, maiores voluptatibus placeat?
            Odio explicabo perferendis eum voluptatum, assumenda quasi reprehenderit beatae officia illum ea?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam dicta aspernatur animi magnam unde ipsum, maiores voluptatibus placeat?
            Odio explicabo perferendis eum voluptatum, assumenda quasi reprehenderit beatae officia illum ea?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam dicta aspernatur animi magnam unde ipsum, maiores voluptatibus placeat?
            Odio explicabo perferendis eum voluptatum, assumenda quasi reprehenderit beatae officia illum ea?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam dicta aspernatur animi magnam unde ipsum, maiores voluptatibus placeat?
            Odio explicabo perferendis eum voluptatum, assumenda quasi reprehenderit beatae officia illum ea?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam dicta aspernatur animi magnam unde ipsum, maiores voluptatibus placeat?
            Odio explicabo perferendis eum voluptatum, assumenda quasi reprehenderit beatae officia illum ea?</div>
        </div> 
    </div>
    )
}

export default AboutMe
