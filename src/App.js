import AboutMe from "./components/AboutMe";
import NavBar from "./components/NavBar";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Favorites from "./components/Favorites";
import Home from "./components/Home";

function App() {
  return (
    <Router>
      <NavBar/>
      <Switch>
          <Route path="/about">
            <AboutMe />
          </Route>
          <Route path="/favorites">
            <Favorites />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
    </Router>

  );
}

export default App;
